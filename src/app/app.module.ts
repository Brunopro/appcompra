import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//Paginas do projeto
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { DetalhePage } from '../pages/detalhe/detalhe';
import { AulaPage } from '../pages/aula/aula';
import { CursosProvider } from '../providers/cursos/cursos';

//Ao criar o primeiro provider do projeto verificar esse importe no imports
import { HttpClientModule } from '@angular/common/http';
import { UserProvider } from '../providers/user/user';

import { IonicStorageModule } from '@ionic/storage';
import { PreferencesProvider } from '../providers/preferences/preferences';
import { FcmProvider } from '../providers/fcm/fcm';
import { Push } from '@ionic-native/push';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    DetalhePage,
    AulaPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    DetalhePage,
    AulaPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CursosProvider,
    UserProvider,
    PreferencesProvider,
    FcmProvider,
    Push
  ]
})
export class AppModule {}
