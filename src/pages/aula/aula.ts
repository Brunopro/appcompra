import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//para conseguir exibir o video pela variavel aula.vide
import { DomSanitizer } from '@angular/platform-browser';

//Interface
import { IAula } from '../../interfaces/IAula';

@IonicPage()
@Component({
  selector: 'page-aula',
  templateUrl: 'aula.html',
})
export class AulaPage {

  aula:IAula;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public domSanitizer: DomSanitizer) {
       this.aula = this.navParams.get('dados');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AulaPage');
  }

}
