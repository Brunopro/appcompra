import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IUser } from '../../interfaces/IUser';
import { UserProvider } from '../../providers/user/user';
import { PreferencesProvider } from '../../providers/preferences/preferences';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {

  public signup : FormGroup;

  constructor(
    private navCtrl: NavController,
    private formBuilder: FormBuilder,
    private preferencesProv: PreferencesProvider,
    private menuCtrl: MenuController,
    private userProv: UserProvider) {

    this.signup = this.formBuilder.group({
      name: ['bruno', Validators.compose([Validators.required, Validators.minLength(3)])],
      email: ['bruno@gmai.com', Validators.compose([Validators.required, Validators.email])],
      password: ['123456', Validators.compose([Validators.required, Validators.minLength(6)])],
      password_confirmation: ['123456', Validators.required]
    }, {validator: this.checkPasswords(this.signup)});
  }

  logForm(){
    
    let user: IUser = {
      name: this.signup.controls.name.value,
      email: this.signup.controls.email.value,
      password: this.signup.controls.password.value,
      password_confirmation: this.signup.controls.password_confirmation.value
    }
    
    this.userProv.signUp(user).subscribe(res => {
      console.log('res', res)
      if(res){
        if(res.token){
          //user preferences
          let userRes:IUser = {
            name: res.name,
            email: res.email,
            token: res.token
          }

          this.preferencesProv.create('user', userRes).then(() => {
            this.navCtrl.setRoot(HomePage);
            this.enableMenuLogged();
          })
        }else {
          console.log('erro de validação', res)
        }
      }else {
        console.log('login não confere', res)
      }

    }, err => {
      console.log('erro', err)
    })


  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
  if(group){
    let pass = group.controls.password.value;
    let confirmPass = group.controls.password_confirmation.value;
  
    return pass === confirmPass ? null : { notSame: true }     
  }
}

  enableMenuLogged(){
    this.menuCtrl.enable(true, 'authenticated');
    this.menuCtrl.enable(false, 'unauthenticated')
  }

}
