import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//Paginas
import { AulaPage } from '../aula/aula';

//Interface
import { ICurso } from '../../interfaces/ICurso'


@IonicPage()
@Component({
  selector: 'page-detalhe',
  templateUrl: 'detalhe.html',
})
export class DetalhePage {

  item:ICurso;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.item = this.navParams.get('item');
  }

  ionViewDidLoad() {

  }

  abreAula(aula){
    this.navCtrl.push(AulaPage, {dados:aula});
  }
}
