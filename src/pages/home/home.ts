import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

//Paginas
import { DetalhePage } from '../detalhe/detalhe';

//Interface
import { ICurso } from '../../interfaces/ICurso'

//Provider
import { CursosProvider } from '../../providers/cursos/cursos';
import { UserProvider } from '../../providers/user/user';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  lista: ICurso[];

  constructor(
    public navCtrl: NavController,
    public cursosProvider: CursosProvider,
    public userProv: UserProvider)
    {

  }

  ionViewDidEnter(){
    //this.lista = cursosProvider.all();
    this.cursosProvider.all().subscribe(res => {
      this.lista = res;
      console.log(this.lista)
    }, erro => {
      console.log("Erro: ", erro.message)
    });

    //Para testar o metodo show() do provider cursos smp que carregar a home page

   this.cursosProvider.show(2).subscribe(res => {
     console.log('usado metodo show', res);
   }, erro => {
     console.log(erro.message)
   } )

  }




  abreDetalhe(item){
    this.navCtrl.push(DetalhePage, {item: item})
  }

}
