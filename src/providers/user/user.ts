import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IUser } from '../../interfaces/IUser';


@Injectable()
export class UserProvider {

  url:string = 'http://localhost:8000/api/';
  headers:any;

  constructor(
    public http: HttpClient,
    ) {
     
  }

  signIn(data:IUser){
    return this.http.post<IUser>(this.url + 'registerteste', data);
  }

  signUp(data:IUser){
    return this.http.post<IUser>(this.url + 'login', data);
  }




}
