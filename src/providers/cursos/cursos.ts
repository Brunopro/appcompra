import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

//interface
import { ICurso } from '../../interfaces/ICurso';
import { listener } from '@angular/core/src/render3/instructions';

@Injectable()
export class CursosProvider {

  constructor(public http: HttpClient) {
    console.log('Hello CursosProvider Provider');
  }

 all(){
   return this.http.get<ICurso[]>('http://localhost:3000/cursos');
 }

 show(id){
   return this.http.get<ICurso>('http://localhost:3000/cursos/' + id)
 }

 add(data: ICurso){
  return this.http.post<ICurso>('http://localhost:3000/cursos', data);
 }

 edit(data: ICurso){
  return this.http.put<ICurso>('http://localhost:3000/cursos/' + data.id, data);
 }

 delete(id){
  return this.http.delete('http://localhost:3000/cursos/' + id);
 }


}
