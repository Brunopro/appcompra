import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage';
import { IUser } from '../../interfaces/IUser';
@Injectable()
export class PreferencesProvider {

  user: IUser

  constructor(public storage:Storage) {

  }

  create(key, data): Promise<any>  {
    return this.storage.ready()
    .then(() => { 
      if(key == 'user'){
        this.user = data;
        return this.storage.set(key, data);
      }
    })

    
  }

  //Vai trazer o user com os dados setados
  get(key): Promise<any> {
    return this.storage.ready()
      .then(() => { 
        return this.storage.get(key)
      })
  }

  // Quando deslogar deve remover do storage
  //usar o preference para deslogar
  remove(key): Promise<boolean> {
    return this.storage.remove(key)
    .then(() => {
      return true
    })
  }


}
